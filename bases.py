#!/usr/bin/env python

from   os import makedirs, system, path
from  xdg.BaseDirectory import xdg_data_dirs
import json

class Initalize:
    def __init__(self):
        self.local_data_dir = xdg_data_dirs[0] + "/flupac/"

        print("flupac: Checking if data dir exists")
        if not path.exists(self.local_data_dir):
            print("flupac: Data dir didn't exist. Created it")
            makedirs(self.local_data_dir)

        print('flupac: Checking if package file list')
        if not path.exists(self.local_data_dir + "pkglist"):
            if self.make_pkg_list():
                print("flupac: Created package cache")

        print('flupac: Checking if file_list.json exists')
        if not path.exists(self.local_data_dir+'file_list.json'):
            self.make_file_list()

        self.file_list = self.get_file_list()
    
    def make_pkg_list(self):
        print('flupac: Called make_pkg_list')
        string = "pacman -Qet | awk '{print $1}' > %spkglist" %(self.local_data_dir)
        if system(string):
                return 1
        return 0

    def get_pkg_list(self):
        """
        @breif Get list of packges
        """
        print('flupac: Called get_pkg_list')
        with open(self.local_data_dir+'pkglist', 'r') as f:
            return [x[:-1] for x in f.readlines()]

    def make_file_list(self):
        """
        @breif Generate a list of files with corresponding package
        """
        print('flupac: Called make_file_list')

        if not path.exists(self.local_data_dir+'file_list.json'):
            file_list = {}
            for package in self.get_pkg_list():
                exec_string = "pacman -Ql %s | awk '{print $2}'" %(package)
                filelist = popen(exec_string).read().split("\n")[:-1]
                file_list[package] = [x for x in filelist if x[-1] != '/']

            with open(self.local_data_dir+'file_list.json', 'w') as f:
                json.dump(file_list, f)

            return file_list
        print('flupac: file_list.json already exists!')
        return 0

    def get_file_list(self):
        '''
        @breif return a dictionary of the file list 
        '''
        file_list = {}
        with open(self.local_data_dir+"file_list.json", 'r') as f:
            jfile = json.load(f)
            return jfile

    def check_file(self, full_path):
        '''
        @breif check if a file exists in our cache
        '''
        for f_list in list(self.file_list.values()):
            if full_path in f_list:
                return True
        return False

if __name__ == "__main__":
    raise Exception("This file is not to be executed directly!")

